#!/usr/bin/env bash
echo "Starting the one time setup of the project"
echo "Downloading the ace build repository"
git clone https://github.com/ajaxorg/ace-builds.git
echo "Creating symbolic link"
ln -s ../../modules/mode-classification.js ./ace-builds/src/mode-classification.js
echo "Setup complete"