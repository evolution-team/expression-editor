## Setup

This repository is intended to be deployable to NPM repository, however that doesn't work yet (so **don't use NPM** to build this project).

To build a functional prototype do the following:

* `git clone https://gitlab.fit.cvut.cz/evolution-team/expression-editor.git`
* `cd expression-editor`
* `./setup.sh`
* open the `./index.html` file

## Todos

* npm packaging
* ? pass variables to mode for proper coloring

## Sources

* https://www.thepolyglotdeveloper.com/2014/08/bypass-cors-errors-testing-apis-locally/
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules
* https://www.npmjs.com/package/brace
* https://www.npmjs.com/package/ace-builds
* http://browserify.org/
* https://wesbos.com/javascript-modules/
* https://makandracards.com/makandra/59151-how-to-use-ace-editor-in-a-webpack-project
