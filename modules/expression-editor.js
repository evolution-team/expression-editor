// import ace from 'brace'
// import {} from 'ace-builds/src/ext-language_tools.js';
// import * as ace from '../node_modules/ace-builds/src/ace.js';

class ExpressionEditor {
    constructor(domId, initialContent, autocompleteCallback, markerCallback) {
        // https://github.com/ajaxorg/ace/wiki/Configuring-Ace
        this.editor = ace.edit(domId, {
            selectionStyle: "text",
            highlightActiveLine: true,
            readOnly: false,
            mergeUndoDeltas: true,
            behavioursEnabled: true,
            wrapBehavioursEnabled: false,
            autoScrollEditorIntoView: false, // this is needed if editor is inside scrollable page,
            copyWithEmptySelection: true, // copy/cut the full line if selection is empty,
            useSoftTabs: false,
            navigateWithinSoftTabs: false,
            enableMultiselect: true,
            newLineMode: "unix",
            showFoldWidgets: false,
            showLineNumbers: true,
            showGutter: true,
            displayIndentGuides: true,
            fontSize: 16,
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
            enableSnippets: false,
            mode: 'ace/mode/classification',
            // theme: 'ace/theme/github',
            value: initialContent,
        });
        this.editor.getSession().setUseWrapMode(true);

        // Autocompletion
        this.autocompleteCallback = autocompleteCallback;
        this.getCompletions = this.getCompletions.bind(this);
        const langTools = ace.require("ace/ext/language_tools");
        const customAutocomplete = {getCompletions: this.getCompletions};
        langTools.setCompleters([customAutocomplete]);

        // Errors and Warnings
        this.markers = [];
        this.markerCallback = markerCallback;
        this.resetTimer = this.resetTimer.bind(this);
        this.getMarkers = this.getMarkers.bind(this);
        this.editor.on("input", this.resetTimer);
    }

    resetTimer() {
        this.clearMarkers();
        clearTimeout(this.timer);
        this.timer = setTimeout(this.getMarkers, 1000);
    }

    getCompletions(editor, session, pos, prefix, callback) {
        this.autocompleteCallback(this.editor.value).then((result) =>
            callback(null, result)
        )
    };

    getMarkers() {
        this.markerCallback(this.editor.value).then((result) => this.setMarkers(result))
    }

    setMarkers(new_markers) {
        this.clearMarkers();
        const Range = ace.require("ace/range").Range;
        let anotations = [];
        new_markers.forEach((marker) => {
            const range = new Range(marker.row, marker.column, marker.endRow, marker.endColumn);
            const type = marker.type; // error, warning, or information
            const text = marker.text;
            this.markers.push(this.editor.getSession().addMarker(range, type + "-marker", "text"));
            anotations.push(
                {
                    row: marker.row,
                    column: marker.column,
                    rowEnd: marker.rowEnd,
                    text: text,
                    type: type,
                }
            )
        });
        this.editor.getSession().setAnnotations(anotations);
    }

    clearMarkers() {
        this.editor.getSession().setAnnotations([]);
        this.markers.forEach((marker) => this.editor.session.removeMarker(marker));
        this.markers = [];
    }
}

// export default ExpressionEditor
