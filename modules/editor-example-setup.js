// import ExpressionEditor from './expression-editor.js';

const initialContent = 'NUMBER a = 2;\n' +
    'NUMBER aa = 20.222 + MAX(3, a);\n' +
    'BOOLEAN bool2 = TRUE || FALSE;\n' +
    'BOOLEAN expr = a == 2;\n' +
    'STRING str = \'tests\\\'trin\\\\g.q"/we\' + $THIS.IDENTIFIER;\n' +
    'STRING TEST = $USERNAME;\n' +
    'STRING full_name = $FIRSTNAME + \' \' + $LASTNAME;\n' +
    'NUMBER pn = $PERSONAL_NUMBER;\n' +
    'AUTO ppn = $PERSONAL_NUMBER;\n' +
    'NUMBER mx = MAX(`\\d+this is a regular expre\\s{2}ion`, `q \\dr.*`);\n' +
    'NUMBER r = !(1+2-3/4*5)^2 == 3 < 7 > 8 <= 9 >= 1 && 1 || 2 ? 3 : 4 % 2;\n' +
    'NUMBER _r2 = !(a++aa+-+-bool2);\n' +
    'str + \'q\' == \'test\' ? SIN(aa) > a : (bool2 ? NULL : 2.3)\n' +
    '\n' +
    'function(var test){ return 0; }\n' +
    '"test bad string"\n' +
    'return should not be emphesized\n' +
    '// comments\n' +
    '/* multiline\n' +
    '\t\t\t\tcomments*/ <- end\n' +
    'good numbers: 123 5400002 21.3 42.4 0 0x0 00 0x45 0b1001 0b0.1 00.1 0x0.1\n' +
    '\n' +
    'bad number: 1e2 2cislo5\n';

async function autocompleteCallback(text) {
    return new Promise((resolve, reject) => {
            resolve([
                {
                    value: "bawr()",
                    score: 1003,
                    meta: "idk man",
                },
                {
                    value: "baar",
                    score: 1002,
                    meta: "custom",
                },
                {
                    value: "baer()",
                    score: 1001,
                    meta: "function(NUMBER, ...)",
                },
                {
                    value: "bar()",
                    score: 800,
                    meta: "constant",
                },
            ]);
        }
    )
}

async function markersCallback(text) {
    return new Promise((resolve, reject) => {
            resolve([
                {
                    row: 4,
                    column: 7,
                    endRow: 4,
                    endColumn: 10,
                    text: "Custom Info Message", // Or the Json reply from the parser
                    type: "information",
                },
                {
                    row: 5,
                    column: 7,
                    endRow: 5,
                    endColumn: 11,
                    text: "Custom Warning Message",
                    type: "warning",
                },
                {
                    row: 6,
                    column: 7,
                    endRow: 7,
                    endColumn: 100,
                    text: "Custom Error Message",
                    type: "error",
                },
            ]);
        }
    )
}

new ExpressionEditor("editor", initialContent, autocompleteCallback, markersCallback);
