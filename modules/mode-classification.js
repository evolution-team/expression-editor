define("ace/mode/classification_highlight_rules", ["require", "exports", "module", "ace/lib/oop", "ace/mode/text_highlight_rules"], function (require, exports, module) {
    "use strict";

    const oop = require("../lib/oop");
    const TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;
    const identifierRegex = "[a-zA-Z\\$_][a-zA-Z\\d\\$_]*";

    const ClassificationHighlightRules = function (options) {
        const keywordMapper = this.createKeywordMapper({
            "variable":
                ".IDENTIFIER|.HIDDEN|.MIN|.MIN_REQUIRED|.MAX|" +
                "$USERNAME|$FIRSTNAME|$LASTNAME|$FULLNAME|$PERSONAL_NUMBER|$THIS", // todo get from endpoint
            "storage.type":
                "AUTO|NUMBER|BOOLEAN|STRING",
            "support.function":
                "MIN|MAX|AVG|SUM|CONCAT|PRODUCT|ABS|RAND|ROUND|FLOOR|CEIL|LOG|LOG10|SQRT|SIN|COS|TAN|ASIN|ACOS|ATAN|" +
                "SINH|COSH|TANH|OR|AND|EQ|NEQ|IF|GT|GTE|LT|LTE|MINUS|MOD|POW|UPLUS|UMINUS|TONUMBER|TOSTRING|TOBOOLEAN",
            // todo also get from endpoint
            "constant.language.boolean": "TRUE|FALSE|NULL"
        }, "identifier");

        const regexDelimeter = "\\`";

        const escapedRe = "\\\\(?:x[0-9a-fA-F]{2}|" + // hex
            "u[0-9a-fA-F]{4}|" + // unicode
            "u{[0-9a-fA-F]{1,6}}|" + // es6 unicode
            "[0-2][0-7]{0,2}|" + // oct
            "3[0-7][0-7]?|" + // oct
            "[4-7][0-7]?|" + //oct
            ".)";

        this.$rules = {
            start: [ // the tokenizer begins from the 'start'!
                comments("start"),
                {
                    token: "string.regexp",
                    regex: "\\/",
                    next: "regex"
                }, {
                    token: "string.regexp",
                    regex: regexDelimeter,
                    next: "regex"
                }, {
                    token: "text",
                    regex: "\\s+|^$",
                    next: "start"
                }, {
                    token: "empty",
                    regex: "",
                    next: "no_regex"
                }
            ],
            no_regex: [
                comments("no_regex"),
                {
                    token: "string", // string in apostrophes
                    regex: "'",
                    next: "qstring"
                }, {
                    token: "constant.numeric", // hexadecimal, octal and binary
                    regex: /0(?:[xX][0-9a-fA-F]+|[oO][0-7]+|[bB][01]+)\b/
                }, {
                    token: "constant.numeric", // decimal integers and floats
                    regex: /(?:\d\d*(?:\.\d*)?|\.\d+)/
                }, {
                    token: ["variable", "punctuation.operator", "variable"], // todo different type
                    regex: "(" + identifierRegex + ")(\\.)(" + identifierRegex + ")",
                }, {
                    token: [keywordMapper, "text", "paren.lparen"],
                    regex: "(" + identifierRegex + ")(\\s+)(\\()",
                    next: "function_arguments"
                }, {
                    token: keywordMapper,
                    regex: identifierRegex
                }, {
                    token: "keyword.operator",
                    regex: /==|=|!=|<=?|>=?|!|&&|\|\||[?:!%+\-*\/^]/,
                    next: "start"
                }, {
                    token: "punctuation.operator",
                    regex: /[,;.]/,
                    next: "start"
                }, {
                    token: "paren.lparen",
                    regex: /[\[({]/,
                    next: "start"
                }, {
                    token: "paren.rparen",
                    regex: /[\])}]/
                }
            ],
            regex: [
                {
                    token: "regexp.keyword.operator",
                    regex: "\\\\(?:u[\\da-fA-F]{4}|x[\\da-fA-F]{2}|.)"
                }, {
                    token: "string.regexp",
                    regex: regexDelimeter,
                    next: "no_regex"
                }, {
                    token: "invalid",
                    regex: /\{\d+\b,?\d*\}[+*]|[+*$^?][+*]|[$^][?]|\?{3,}/
                }, {
                    token: "constant.language.escape",
                    regex: /\(\?[:=!]|\)|\{\d+\b,?\d*\}|[+*]\?|[()$^+*?.]/
                }, {
                    token: "constant.language.delimiter",
                    regex: /\|/
                }, {
                    token: "constant.language.escape",
                    regex: /\[\^?/,
                    next: "regex_character_class"
                }, {
                    token: "empty",
                    regex: "$",
                    next: "no_regex"
                }, {
                    defaultToken: "string.regexp"
                }
            ],
            regex_character_class: [
                {
                    token: "regexp.charclass.keyword.operator",
                    regex: "\\\\(?:u[\\da-fA-F]{4}|x[\\da-fA-F]{2}|.)"
                }, {
                    token: "constant.language.escape",
                    regex: "]",
                    next: "regex"
                }, {
                    token: "constant.language.escape",
                    regex: "-"
                }, {
                    token: "empty",
                    regex: "$",
                    next: "no_regex"
                }, {
                    defaultToken: "string.regexp.charachterclass"
                }
            ],
            function_arguments: [
                {
                    token: "variable.parameter",
                    regex: identifierRegex
                }, {
                    token: "punctuation.operator",
                    regex: "[, ]+"
                }, {
                    token: "punctuation.operator",
                    regex: "$"
                }, {
                    token: "empty",
                    regex: "",
                    next: "no_regex"
                }
            ],
            qstring: [
                {
                    token: "constant.language.escape",
                    regex: escapedRe
                }, {
                    token: "string",
                    regex: "\\\\$",
                    consumeLineEnd: true
                }, {
                    token: "string",
                    regex: "'|$",
                    next: "no_regex"
                }, {
                    defaultToken: "string"
                }
            ]
        };

        this.normalizeRules();
    };

    oop.inherits(ClassificationHighlightRules, TextHighlightRules);

    function comments(next) {
        return [
            {
                token: "comment", // multi line comment
                regex: /\/\*/,
                next: [
                    {token: "comment.doc.tag.storage.type", regex: "\\b(?:TODO|FIXME|XXX|HACK)\\b"},
                    {token: "comment", regex: "\\*\\/", next: next || "pop"},
                    {defaultToken: "comment", caseInsensitive: true},
                ]
            }, {
                token: "comment",
                regex: "\\/\\/",
                next: [
                    {token: "comment.doc.tag.storage.type", regex: "\\b(?:TODO|FIXME|XXX|HACK)\\b"},
                    {token: "comment", regex: "$|^", next: next || "pop"},
                    {defaultToken: "comment", caseInsensitive: true},
                ]
            }
        ];
    }

    exports.ClassificationHighlightRules = ClassificationHighlightRules;
});

define("ace/mode/classification", ["require", "exports", "module", "ace/lib/oop", "ace/mode/text", "ace/mode/classification_highlight_rules", "ace/mode/behaviour/cstyle"], function (require, exports, module) {
    "use strict";

    const oop = require("../lib/oop");
    const TextMode = require("./text").Mode;
    const ClassificationHighlightRules = require("./classification_highlight_rules").ClassificationHighlightRules;
    const CstyleBehaviour = require("./behaviour/cstyle").CstyleBehaviour;

    const Mode = function () {
        this.HighlightRules = ClassificationHighlightRules;
        this.$behaviour = new CstyleBehaviour();
    };
    oop.inherits(Mode, TextMode);

    (function () {
        // setting up the comment style allows for commenting and uncommenting using (standard) Ctrl+/ shortcut
        this.lineCommentStart = "//";
        this.blockComment = {start: "/*", end: "*/"};
        // will create second quote on writing the first one
        this.$quotes = {"'": "'", "`": "`"};
        this.$id = "ace/mode/classification";
    }).call(Mode.prototype);

    exports.Mode = Mode;
});
(function () {
    window.require(["ace/mode/classification"], function (m) {
        if (typeof module == "object" && typeof exports == "object" && module) {
            module.exports = m;
        }
    });
})();
